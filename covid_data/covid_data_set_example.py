#!/usr/bin/env python
# coding: utf-8
"""
covid_data_set_example.py
developed with a jupyter notebook
to demonstrate
* retrieving json data from an internet data source
* creating a database based on that data
  * TODO: use correct types
* insert data into database

to be broken apart and put into modules for actual use

"""

import pandas


url = ("https://covid.ourworldindata.org"
       "/data/owid-covid-data.json")
data = pandas.read_json(url)
df = pandas.DataFrame(data, columns=data.keys())
dft = df.T

unique_continents = set(f"{d}" for d in dft['continent'])

all_unique_data_keys = set()
for country_datas in dft["data"]:
    for country_data in country_datas:
        all_unique_data_keys.update(
            [k for k in country_data.keys()]
        )
all_unique_data_keys = sorted(all_unique_data_keys)


cd_cols = str()
for k in all_unique_data_keys:
    # TODO: determine type
    cd_cols += f"    {k} text,\n"

string_meta_cols = str()
for c in dft.columns:
    if c not in ("continent", "data"):
        # TODO: determine type
        string_meta_cols += f"{c} text,\n"

# generator expression  -- with a filter (for a tuple)
# c for c in dft.columns if c not in ("continent", "data")

# list comprehension
# [x for x in ['x', 'y', 'z']]

create_database = f"""
create table if not exists continents(
    continent_id serial primary key,
    continent text
);

create table if not exists countries(
    country_id serial primary key,
    country text,
    {string_meta_cols}
    continent_id int,
    foreign key (continent_id)
        references continents(continent_id)
);

create table if not exists country_data(
    country_data_id serial primary key,
    {cd_cols}
    country_id int,
    foreign key (country_id)
        references countries(country_id)
        on delete cascade
);
"""


from sqlalchemy import create_engine
from sqlalchemy import text

database_name = "test_zero"

engine = create_engine(
    "postgresql://cmr:wickchad@localhost",
    isolation_level='AUTOCOMMIT',
)

connection = engine.connect()
connection.commit()
connection.execute(text(f"create database {database_name}"))
connection.close()


engine = create_engine(
    f"postgresql://cmr:wickchad@localhost/{database_name}",
)
connection = engine.connect()
connection.execute(text(create_database))
connection.commit()

# used while developing
# connection.rollback()

dfcontinents = pandas.read_sql_table(
    "continents",
    connection,
    index_col="continent"
)

dcontinents = dfcontinents.to_dict(orient="records")

unique_continents_pre = "'), ('".join(unique_continents)
insert_continents_sql = (
    "insert into continents "
    f"(continent) values ('{unique_continents_pre}');"
)
connection.execute(text(insert_continents_sql))
connection.commit()


dft.to_sql("direct", connection)

for n, f in dft.iterrows():
    data_column = f.loc["data"]
    no_data_columns = [i for i in f.index if i not in ("continent", "data")]

    db_columns = ", ".join(no_data_columns)
    db_columns += ", country, continent_id"
    db_values = list()

    continent_column = f.loc["continent"]
    continent_id = dfcontinents.loc[f"{continent_column}"]["continent_id"]

    for column in no_data_columns:
        column_data = f"{f.loc[column]}"
        column_data = column_data.replace("'", "_")
        db_values.append(column_data)
    db_values_string = "', '".join(db_values)
    db_values_string += f"', '{n}', '{continent_id}"
    insert_sql = (
        "insert into countries "
        f"({db_columns}) values ('{db_values_string}')"
    )
    connection.execute(text(insert_sql))


connection.commit()


dfcountries = pandas.read_sql_table(
    "countries",
    connection,
    index_col="country"
)

for n, f in dft.iterrows():
    country_id = dfcountries.loc[n]["country_id"]
    data_columns = f.loc["data"]
    db_columns = ", ".join(data_columns[0].keys())
    db_values = "', '".join(f"{v}" for v in data_columns[0].values())
    insert_sql = f"""
    insert into country_data
    ({db_columns}, country_id)
    values
    ('{db_values}', '{country_id}')
    """

    for data_column in data_columns:
        db_columns = (
            ", ".join(data_column.keys())
            + ", country_id"
        )
        db_values = (
            "'"
            + "', '".join(f"{v}" for v in data_column.values())
            + f"', {country_id}"
        )
        insert_sql = (
            "insert into country_data "
            f"({db_columns}) values ({db_values})"
        )
        connection.execute(text(insert_sql))
