"""
covid_data_ourworldindata.py

"""
from pathlib import Path
from covid_data_ourworldindata_retrieve import retrieve
from covid_data_ourworldindata_create import create
from covid_data_ourworldindata_insert import insert
from covid_data_ourworldindata_insert import settings

DEV = settings.DEV
cache_path = Path(settings.cache_file)


def covid_data_ourworldindata():
    """run all the parts in order
    """
    use_cache = False
    if cache_path.exists() and DEV:
        print("using cached json")
        use_cache = True
    print("retrieving data")
    covid_data = retrieve(use_cache, cache_path=cache_path)
    print("data retrieved")

    print("creating database")
    create(covid_data)
    print("database created")

    print("inserting data")
    insert(covid_data)
    print("data inserted")

    print("done")


if __name__ == "__main__":
    covid_data_ourworldindata()
