# covid_data_ourworldindata
for running covid_data
* make sure postgresql is running on default TCP 5432
* make sure a database user exists that can
    * create databases
    * create tables in created datbases
    * insert data in to created tables in created databases
    * eventually also select the data
* copy creds_example.py to creds.py and correct
    * database_user
    * database_pass
* make sure covid_data_ourworldindata_settings.py
    * database_host (should probably be localhost as is)
    * database_name
    - can change name for multiple tests
    - or drop the database between runs
* get a virtual env with the right packages
    * pandas
    * sqlalchemy
    * psycopg2
* python3 covid_data_ourworldindata.py
* watch the data inserted
* select data

`select country, location, continent from countries join continents on countries.continent_id = continents.continent_id;`
