from dataclasses import dataclass


@dataclass
class Settings:
    database_host: str = "localhost"
    database_name: str = "test_two"
