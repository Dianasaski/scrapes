"""
covid_data_ourworldindata_retrieve.py

"""
import json

import requests
import pandas


def retrieve(use_cache, cache_path):
    """
    """
    url = (
        "https://covid.ourworldindata.org"
        "/data/owid-covid-data.json"
    )
    if use_cache:
        df = pandas.read_json(cache_path)
    else:
        print(f"fetching data from {url}")
        response = requests.get(url)
        print("data fetched")
        # direct url is also possible
        # but caching was problematic
        # data = pandas.read_json(url)
        data = pandas.read_json(response.text)
        print("creating data frame")
        df = pandas.DataFrame(data, columns=data.keys())
        print("creating cache file")
        # for some reason this does not read back nicely
        # perhaps there is a better way to serialise dataframes ?
        # df.to_json(cache_path)
        # reverting to writing directly from requests response
        cache_path.write_text(response.text)

    print("transposing data frame and returning")
    return df.T
