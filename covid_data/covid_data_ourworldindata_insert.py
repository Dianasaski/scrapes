"""
covid_data_ourworldindata_insert.py

"""
import pandas
from sqlalchemy import create_engine
from sqlalchemy import text

from creds import Creds
from covid_data_ourworldindata_settings import Settings
creds = Creds()
settings = Settings()


database_user = creds.database_user
database_pass = creds.database_pass
database_host = settings.database_host
database_name = settings.database_name


def insert(covid_data):
    print("creating engine for inserting data")
    database_engine = create_engine(
        f"postgresql://{database_user}"
        f":{database_pass}@{database_host}"
        f"/{database_name}"
    )
    print("creating connection for inserting data")
    database_connection = database_engine.connect()

    def run_sql(sql):
        # debugging this is a lot
        # print("running insert data sql")
        database_connection.execute(text(sql))
        # print("insert data sql committed:")
        # print(sql)

    def commit_data():
        database_connection.commit()

    print("determining continents")
    unique_continents = set(
        f"{continent}" for continent in covid_data['continent']
    )
    unique_continents_pre = "'), ('".join(unique_continents)
    insert_continents_sql = (
        "insert into continents "
        f"(continent) values ('{unique_continents_pre}')"
    )
    print("inserting continents")
    run_sql(insert_continents_sql)
    print("committing continents")
    commit_data()

    print("retrieving continents from database into pandas dataframe")
    dfcontinents = pandas.read_sql_table(
        "continents",
        database_connection,
        index_col="continent"
    )

    print("looping through covid data to insert country meta data")
    for covid_data_index, covid_data_row in covid_data.iterrows():
        print(f"working on {covid_data_index}")
        no_data_columns = list()
        for index in covid_data_row.index:
            if index not in ("continent", "data"):
                no_data_columns.append(index)

        insert_columns = ", ".join(no_data_columns)
        insert_columns += ", country, continent_id"
        insert_values = list()

        continent_column = covid_data_row.loc["continent"]
        continent_id = dfcontinents.loc[f"{continent_column}"]["continent_id"]

        for column in no_data_columns:
            column_data = f"{covid_data_row.loc[column]}"
            column_data = column_data.replace("'", "_")
            insert_values.append(column_data)
        insert_values_string = "', '".join(insert_values)
        insert_values_string += f"', '{covid_data_index}', '{continent_id}"
        insert_sql = (
            "insert into countries "
            f"({insert_columns}) values ('{insert_values_string}')"
        )
        print(f"inserting {covid_data_index}")
        run_sql(insert_sql)
        print(f"inserted {covid_data_index}")

    print("retrieving countries from database into pandas dataframe")
    dfcountries = pandas.read_sql_table(
        "countries",
        database_connection,
        index_col="country"
    )

    print("looping through covid data to insert country data")
    for covid_data_index, covid_data_row in covid_data.iterrows():
        country_id = dfcountries.loc[covid_data_index]["country_id"]
        country_datas = covid_data_row.loc["data"]
        print(f"working on {covid_data_index} with id {country_id}")

        insert_columns = ", ".join(country_datas[0].keys())
        insert_values = "', '".join(f"{v}" for v in country_datas[0].values())
        insert_sql = (
            f"insert into country_data"
            "({insert_columns}, country_id) values"
            f"('{insert_values}', '{country_id}')"
        )

        print(f"looping through all data and inserting for {covid_data_index}")
        for country_data in country_datas:
            insert_columns = (
                ", ".join(country_data.keys())
                + ", country_id"
            )
            insert_values = (
                "'"
                + "', '".join(f"{v}" for v in country_data.values())
                + f"', {country_id}"
            )
            insert_sql = (
                "insert into country_data "
                f"({insert_columns}) values ({insert_values})"
            )
            run_sql(insert_sql)
        print(f"inserted all data for {covid_data_index}")

    print("committing country data")
    commit_data()
    print("closing database connection")
    database_connection.close()
