from dataclasses import dataclass


@dataclass
class Creds:
    database_user: str = "database_username"
    database_pass: str = "database_password"
