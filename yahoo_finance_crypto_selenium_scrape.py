from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

chrome_options = Options()
chrome_options.add_argument("--headless")

driver_service = Service("/home/cmr/Downloads/selenium/chromedriver")
driver_service = Service(
    executable_path="/home/cmr/Downloads/selenium/chromedriver")
driver_service = Service()
driver = webdriver.Chrome(service=driver_service, options=chrome_options)

driver.get("https://fr.finance.yahoo.com/crypto-monnaies")

buttons = driver.find_elements(By.XPATH, "//button")
b1 = buttons[1]
b1.click()

tbodies = driver.find_elements(By.XPATH, "//tbody")
tbody = tbodies[0]
trs = tbody.find_elements(By.XPATH, "./tr")

for tr in trs:
    tda = tr.find_element(By.XPATH, "./td/a")
    print(f"{tda.text}", end=",")
    tdss = tr.find_elements(By.XPATH, "./td/fin-streamer/span")
    print(",".join(s.text for s in tdss))
